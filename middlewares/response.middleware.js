const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   try {
       if(res.data) {
        return res.status(200).send();
        next();
       }
   } catch {err} {
       return `${res.err}`
   }

}

exports.responseMiddleware = responseMiddleware;