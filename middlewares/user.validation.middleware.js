const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        const {id, firstName, lastName, email, phoneNumber, password } = req.body;

        if(!firstName || !lastName ) {
            throw new Error('Enter valid firstName');
        }

        if(!lastName ) {
            throw new Error('Enter valid lastName');
        }

        if(!email || !EMAIL_REGEXP.test(email) || !email.includes('@gmail.com')) {
            throw new Error('Enter valid email');
        }

        if(!phoneNumber || !NUMBER_REGEXP.test(phoneNumber)) {
            throw new Error('Enter valid number');
        }

        if ( !password|| !password.length >= 3) {
            throw new Error('Enter valid password')
        }
        if(Object.keys(req.body).length == 0) {
            throw new Error('Enter data');
        }

        next();

    }catch (e) {
        res.status(400).json(e.message);
    }
};

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        const { id } = req.params;
        const fields = Object.keys(req.body);

        if (!fields.every(key => userKeys.indexOf(key) >= 0) || id || !UserService.findUsersById(id) || Object.keys(fields).length === 0) {
            throw new Error('Validation error status 1');
        } else {
            for (let key in req.body) {
                if (key === 'email' && (!req.body[key].includes('@gmail.com') || !EMAIL_REGEXP.test(req.body[key]))) {
                    throw new Error('Validation error status 2')
                    break;
                }
                if (key === 'password' && req.body[key].length < 3) {
                    throw new Error('Validation error status 3')
                    break;
                }
                if (key === 'phoneNumber' && !NUMBER_REGEXP.test(req.body[key])) {
                    throw new Error('Validation error status 4')
                    break;
                }
            }
            next();
        }
    }catch (e) {
        res.status(400).json(e.message);
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;