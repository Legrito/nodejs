const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    findFighters() {
        return  FightRepository.getAll();
    }

    findFighterById(id) {
        const fighter = this.search({ id });

        return fighter;
    }

    createFighter(data) {
         return  FightRepository.create(data);
    }

    updateFighter(id, data) {
        return FightRepository.update(id, data);
    }

    deleteFighter(id) {
       return FightRepository.delete(id);
    }

    search(search) {
        const searchFighter = FightRepository.getOne(search);
        if(!searchFighter) {
            return null;
        }
        return searchFighter;
    }
    //----------------
}

module.exports = new FighterService();